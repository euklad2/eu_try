﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular_ASPNETCore_Seed.Models
{
    public class Message
    {
        public string Data { get; set; }
    }

    public class Paths
    {
        public string[] Data { get; set; }
    }
}
