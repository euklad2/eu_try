using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Angular_ASPNETCore_Seed.Models;
using System.IO;
using Newtonsoft.Json;

namespace Angular_ASPNETCore_Seed.Apis
{
    [Route("api")]
    public class MessagesController : Controller
    {
        [Route("messages")]
        [HttpGet]
        [ProducesResponseType(typeof(Message), 200)]
        public async Task<ActionResult> Messages()
        {
            //Simulate async process
            return await Task.Run(() =>
            {
                var msg = new Message { Data = "Hello World" };
                return Ok(msg);
            });
        }

        [Route("paths/{mapping}/{route}")]
        [HttpGet]
        [ProducesResponseType(typeof(Paths), 200)]
        public ActionResult GetPaths(string mapping, string route)
        {
            string filteredRoute = ConvertRoute(route);
            var msg = ResolvePaths(mapping, filteredRoute);
            return Ok(msg);
        }

        [Route("mappings/{mapping}")]
        [HttpGet]
        [ProducesResponseType(typeof(string[]), 200)]
        public ActionResult GetMappings(string mapping)
        {
            var dict = GetDictCache();

            var msg = dict.Keys.Where(k => k.StartsWith(mapping)).ToArray();
            return Ok(msg);
        }

        private string ConvertRoute(string route)
        {
            if (route == "null") return "";

            return route;
        }

        public Paths ResolvePaths(string mapping, string route)
        {
            var result = new Paths();
            Dictionary<string, List<string>> dict = GetDictCache();
            result.Data = ResolveMappingPaths(mapping, route, new string[] { }, dict);
            return result;
        }

        private static Dictionary<string, List<string>> GetDictCache()
        {
            string text;

            using (var sw = new StreamReader(new FileStream(@"c:\temp\dict.json", FileMode.Open)))
            {
                text = sw.ReadToEnd();
            }

            Dictionary<string, List<string>> dict = (Dictionary<string, List<string>>)JsonConvert.DeserializeObject(text, typeof(Dictionary<string, List<string>>));
            return dict;
        }

        public string[] ResolveMappingPaths(string mapping, string mappingRoute, string[] defaultRoutes, Dictionary<string, List<string>> dict)
        {
            if (!dict.ContainsKey(mapping))
            {
                //throw new MappingNotFoundException();
                return new string[] { };
            }

            var paths = dict[mapping];

            List<string> routes = new List<string>();
            routes.Add(mappingRoute);
            routes.AddRange(defaultRoutes);

            foreach (var route in routes)
            {
                paths = ReduceByRoute(paths, route);

                if (paths.Count < 2)
                {
                    break;
                }
            }

            //if (paths.Count == 0)
            //{
            //    throw new MappingNotFoundException();
            //}

            //if (paths.Count > 1)
            //{
            //    throw new MappingRouteResolutionException();
            //}

            return paths.ToArray();
        }

        private List<string> ReduceByRoute(List<string> paths, string route)
        {
            var result = new List<string>();
            var routeParts = route.Split(new char[] { '/', '\\', '.' }).Where(r => !string.IsNullOrEmpty(r));

            foreach (var pathData in paths)
            {
                string path = pathData;
                int index = 0;

                foreach (var part in routeParts)
                {
                    index = path.IndexOf(part, index);

                    if (index == -1)
                    {
                        break;
                    }
                }

                if (index > -1)
                {
                    result.Add(pathData);
                }
            }

            return result;
        }
    }
}