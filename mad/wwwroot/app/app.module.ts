import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';

//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { MdButtonModule, MdCheckboxModule, MdAutocompleteModule } from '@angular/material';

@NgModule({
    imports: [BrowserModule, AppRoutingModule, CoreModule/*, MdButtonModule, MdCheckboxModule, MdAutocompleteModule*/ ],
  declarations: [ AppComponent, AppRoutingModule.components],
  bootstrap: [AppComponent],
  //exports: [MdAutocompleteModule]
})
export class AppModule { }