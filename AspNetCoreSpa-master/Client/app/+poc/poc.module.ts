import { NgModule } from '@angular/core';

import 'rxjs/add/observable/concat';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/observable/fromEvent';

import { SharedModule } from '../shared/shared.module';
import { routing } from './poc.routes';
import { PocComponent } from './poc.component';

@NgModule({
    imports: [
        routing,
        SharedModule
    ],
    providers: [],
    declarations: [
        PocComponent
    ]
})
export class PocModule { }
