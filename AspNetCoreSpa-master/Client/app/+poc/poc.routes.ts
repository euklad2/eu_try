import { Routes, RouterModule } from '@angular/router';

import { PocComponent } from './poc.component';

const routes: Routes = [
    {
        path: '', component: PocComponent
    }
];

export const routing = RouterModule.forChild(routes);
