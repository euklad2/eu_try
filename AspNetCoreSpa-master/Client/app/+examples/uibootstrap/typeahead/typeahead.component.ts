import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';

import { WikipediaService } from './wikipedia.service';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';

const states = ['Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado',
  'Connecticut', 'Delaware', 'District Of Columbia', 'Federated States Of Micronesia', 'Florida', 'Georgia',
  'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine',
  'Marshall Islands', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana',
  'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
  'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Palau', 'Pennsylvania', 'Puerto Rico', 'Rhode Island',
  'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virgin Islands', 'Virginia',
  'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];

@Component({
  selector: 'appc-typeahead',
  templateUrl: './typeahead.component.html',
  providers: [WikipediaService]
})
export class TypeaheadComponent {
  public _searching: boolean;
  public model: string;
  public wikiModel: string;

  public fieldModel: string;
  public pathsModel: string;
  public paths: string;

  public mapping: string;

  constructor(public _service: WikipediaService, private _http: Http) {
  }


 
    public fieldSearch = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((term: any) => { this._searching = term.length > 0; })
            .switchMap(term => term === '' ? Observable.of([]) : this._service.search2(term))
            .do(() => {
                this._searching = false;
                this.loadPaths();
            })

    public onClickMe() {
        this.loadPaths();
    }

    public loadPaths() {
        let p = this.pathsModel;

        if (p == '' || p == undefined) {
            p = 'null';
        }

        //let data = this._http.get('http://localhost:5000/api/paths/' + this.fieldModel + '/' + p)
        //    // tslint:disable-next-line:whitespace
        //    .map((res: Response) => <string[]>res.json())
        //    .do(res => {
        //        this.paths = JSON.stringify(res);
        //        //this.pathsObservable = this.paths;
        //    })
        //    .catch(error => {
        //        console.error(error);
        //        return Observable.throw(error.json());
        //    });

        let data = this._service.searchPaths(this.fieldModel, p);

        data.subscribe((val) => {
            this.paths = val.join('\r\n')
        });

        let p2 = this.pathsModel;

        if (p2 == undefined) {
            p2 = '';
        }

        this.mapping = p2.replace(".", "\\") + "\\" + this.fieldModel;
    }


  public search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : states.filter(v => new RegExp(term, 'gi').test(v)).splice(0, 10));

  public searchWiki = (text$: Observable<string>) =>
    text$
      .debounceTime(300)
      .distinctUntilChanged()
      .do((term: any) => { this._searching = term.length > 0; })
      .switchMap(term => term === '' ? Observable.of([]) : this._service.search(term))
      .do(() => { this._searching = false; })
}
