import { Injectable } from '@angular/core';
import { Jsonp, URLSearchParams, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WikipediaService {
    constructor(public _jsonp: Jsonp, private _http: Http) { }

    public search(term: string) {
        const wikiUrl = 'https://en.wikipedia.org/w/api.php';
        const params = new URLSearchParams();
        params.set('search', term);
        params.set('action', 'opensearch');
        params.set('format', 'json');
        params.set('callback', 'JSONP_CALLBACK');

        return this._jsonp
            .get(wikiUrl, { search: params })
            // tslint:disable-next-line:whitespace
            .map(response => <string[]>response.json()[1]);
    }

    public search2old(term: string) {
        const url = 'http://localhost:5000/api/mappings/';
        

        let data = this._jsonp
            .get(url + term)
            // tslint:disable-next-line:whitespace
            .map(
                response => <string[]>response.json()
            );

        return data;
    }

    public search2(term: string): Observable<string[]> {
        return this._http.get('http://localhost:5000/api/mappings/' + term)
            // tslint:disable-next-line:whitespace
            .map((res: Response) => <string[]>res.json())
            .do(res => console.log('Mapping Data Object: ' + JSON.stringify(res)))
            .catch(error => {
                console.error(error);
                return Observable.throw(error.json());
            });
    }

    public searchPaths(term: string, path: string): Observable<string[]> {
        return this._http.get('http://localhost:5000/api/paths/' + term + '/' + path)
            // tslint:disable-next-line:whitespace
            .map((res: Response) => <string[]>res.json())
            .do(res => console.log('Mapping Data Object: ' + JSON.stringify(res)))
            .catch(error => {
                console.error(error);
                return Observable.throw(error.json());
            });
    }
}
